<?php 

// generate list
$list = array();
echo '<p>List:</p>';

for ($i=0; $i<400; $i++) {
	$rndYear = rand(1900, 2000);
	$list[] = $rndYear;
	echo $rndYear.'<br />';
}


// Create a year index array 
$ageIndexedList = array();
foreach ($list as $item) {
	if (!isset($ageIndexedList[$item])) {
		$ageIndexedList[$item] = 1;
	} else {
		$ageIndexedList[$item]++;
	}
}


// just sort the results, low overhead
arsort($ageIndexedList);

// first item should be the one
foreach($ageIndexedList as $year=>$amount) {
	echo '<p>&nbsp;</p>';
	echo '<p><b>Winning year is:</b> '.$year.' with <b>'.$amount.'</p> ';
	break;
}

echo '<p>&nbsp;</p><p>Raw Data Output:</p>';
echo '<pre>';

print_r($ageIndexedList);

echo '</pre>';



?>